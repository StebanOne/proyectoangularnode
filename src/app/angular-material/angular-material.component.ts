import { Component, OnInit } from '@angular/core';
import { ServerService } from '../server.service'
import { Recipe } from '../model/recipe-model'

@Component({
  selector: 'app-material',
  templateUrl: './angular-material.component.html',
  styleUrls: ['./angular-material.component.css']
})
export class AngularMaterialComponent implements OnInit {

showBtn: number;
allow = false;
  recipeCollection:Recipe[]
  constructor( private serverService: ServerService) {}
  ngOnInit() {
   this.serverService.getSevers().subscribe((recipes: Recipe[])=>{
      // console.log(recipes);
      this.recipeCollection = recipes;
   } 
  );
 
  }
  toggle(index: number){
    this.showBtn = index;
    

  }
  onDelete(index:number) {
    console.log(index);
  }

}