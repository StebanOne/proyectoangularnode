import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // tslint:disable-next-line:typedef
  ngOnInit(){
    // tslint:disable-next-line:no-unused-expression prefer-const
    let ctx = document.getElementById('myChart');
    // @ts-ignore
    const myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        datasets: [{
          label: 'Stock de Productos',
          backgroundColor: ['#6bf1ab', '#63d69f', '#438c6c', '#509c7f', '#1f794e', '#34444c', '#90CAF9', '#64B5F6', '#42A5F5', '#2196F3', '#0D47A1'],
          borderColor: ['black'],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          responsive: true,
          maintainAspectRatio: false,
        }
      }
    });
    const url = 'http://localhost/apirest_php/entidades.php';
    fetch(url)
      .then( response => response.json() )
      .then( datos => mostrar(datos) )
      .catch( error => console.log(error) );


    const mostrar = (entidades) => {
      entidades.forEach(element => {
        myChart.data.labels.push(element.nombre);
        myChart.data.datasets[0].data.push(element.id);
        myChart.update();
      });
      console.log(myChart.data);
    };
  }
}
