import {Component, OnInit, ViewChild} from '@angular/core';
import { AuthService } from '@auth/auth.service';
import {takeUntil} from "rxjs/operators";
import {ModalComponent} from "@admin/admin/components/modal/modal.component";
import {MatSort} from "@angular/material/sort";
import {UsersService} from "@admin/admin/services/users.service";
import {MatDialog} from "@angular/material/dialog";
import {Subject} from "rxjs";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.scss']
})
export class AyudaComponent implements OnInit {
  dataSource = new MatTableDataSource();

  private destroy$ = new Subject<any>();
  @ViewChild(MatSort) sort: MatSort;
  constructor(private userSvc: UsersService, private dialog: MatDialog) {}
  ngOnInit(): void {
    this.userSvc.getAll().subscribe((users) => {
      this.dataSource.data = users;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
  onDelete(userId: number): void {
    if (window.confirm('Seguro quieres eliminar a este usuario!')) {
      this.userSvc
        .delete(userId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((res) => {
          window.alert('Usuario Eliminado');
          // Update result after deleting the user.
          this.userSvc.getAll().subscribe((users) => {
            this.dataSource.data = users;
          });
        });
    }
  }

  onOpenModal(user = {}): void {
    console.log('User->', user);
    const dialogRef = this.dialog.open(ModalComponent, {
      height: '400px',
      width: '600px',
      hasBackdrop: true,
      data: { title: 'Registre usuario', user },
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`, typeof result);
      // Update result after adding new user.
      this.userSvc.getAll().subscribe((users) => {
        this.dataSource.data = users;
      });
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }
}
