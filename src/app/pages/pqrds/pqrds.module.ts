import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PqrdsRoutingModule } from './pqrds-routing.module';
import { PqrdsComponent } from './pqrds.component';


@NgModule({
  declarations: [PqrdsComponent],
  imports: [
    CommonModule,
    PqrdsRoutingModule
  ]
})
export class PqrdsModule { }
