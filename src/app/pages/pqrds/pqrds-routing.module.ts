import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PqrdsComponent } from './pqrds.component';

const routes: Routes = [{ path: '', component: PqrdsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PqrdsRoutingModule { }
