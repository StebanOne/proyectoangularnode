
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { CommonModule } from '@angular/common';

import { CargaRoutingModule } from './carga-routing.module';
// To use angular-file-uploader from node-modules remove paths from tsconfig.base.json in root.
/*
"paths": {
      "angular-file-uploader": [
        "dist/angular-file-uploader"
      ],
      "angular-file-uploader/*": [
        "dist/angular-file-uploader/*"
      ]
    }
*/
import { CargaComponent } from './carga.component';



@NgModule({
  declarations: [
    CargaComponent,
  ],
  imports: [
    AngularFileUploaderModule,
    CommonModule,
    CargaRoutingModule
  ],
  providers: [],
  bootstrap: [CargaComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CargaModule { }