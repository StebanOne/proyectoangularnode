import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckLoginGuard } from '@shared/guards/check-login.guard';
import { HomeModule } from './pages/home/home.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'notFound',
    loadChildren: () =>
      import('./pages/not-found/not-found.module').then(
        (m) => m.NotFoundModule
      ),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./pages/admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'perfil',
    loadChildren: () =>
      import('@admin/admin/perfil/perfil.module').then((m) => m.PerfilModule),
    canActivate: [],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/auth/login/login.module').then((m) => m.LoginModule),
    canActivate: [CheckLoginGuard],
  },
  {
    path: 'ayuda',
    loadChildren: () =>
      import('./pages/ayuda/ayuda.module').then((m) => m.AyudaModule),
    canActivate: [],
  },
  {
    path: 'carga',
    loadChildren: () =>
      import('./pages/carga/carga.module').then((m) => m.CargaModule),
    canActivate: [],
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('@admin/admin/perfil/perfil.module').then((m) => m.PerfilModule),
    canActivate: [],
  },
  {
    path: 'pqrds',
    loadChildren: () =>
      import('./pages/pqrds/pqrds.module').then((m) => m.PqrdsModule),
    canActivate: [],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
