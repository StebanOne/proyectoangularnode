import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { Users } from '../entity/Users';
import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import { validate } from 'class-validator';

class AuthController {
  static login = async (req: Request, res: Response) => {
    const { username, password } = req.body;

    if (!(username && password)) {
      return res.status(400).json({ message: ' Se requiere Username y Password' });
    }

    const userRepository = getRepository(Users);
    let user: Users;

    try {
      user = await userRepository.findOneOrFail({ where: { username } });
    } catch (e) {
      return res.status(400).json({ message: ' Username o password incorrectos!' });
    }

    // Check password

    if (!user.checkPassword(password)) {
      return res.status(400).end({ message: 'Username o Password incorretos!' });

    }

    // const token = jwt.sign({ userId: user.id, username: user.username }, config.jwtSecret, { expiresIn: '2m' });

    res.json({ message: 'OK', userId: user.id, role: user.role });
  }


  static changePassword = async (req: Request, res: Response) => {
    const { userId } = res.locals.jwtPayload;
    const { oldPassword, newPassword } = req.body;

    if (!(oldPassword && newPassword)) {
      res.status(400).json({ message: 'Se requiere clave antigua y nueva' });
    }

    const userRepository = getRepository(Users);
    let user: Users;

    try {
      user = await userRepository.findOneOrFail(userId);
    } catch (e) {
      res.status(400).json({ message: 'Algo salio mal!' });
    }

    if (!user.checkPassword(oldPassword)) {
      return res.status(401).json({ message: 'Mira tu antigua contraseña!' });
    }

    user.password = newPassword;
    const validationOps = { validationError: { target: false, value: false } };
    const errors = await validate(user, validationOps);

    if (errors.length > 0) {
      return res.status(400).json(errors);
    }

    // Hash password
    user.hashPassword();
    userRepository.save(user);

    res.json({ message: 'Password cambio!' });
  }
}
export default AuthController;
