import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { Users } from '../entity/Users';
import { validate } from 'class-validator';

export class UserController {
  static getAll = async (req: Request, res: Response) => {
    const userRepository = getRepository(Users);
    let users;

    try {
      users = await userRepository.find({ select: ['id', 'username', 'role'] });
    } catch (e) {
      res.status(404).json({ message: 'Algo salio mal!' });
    }

    if (users.length > 0) {
      res.send(users);
    } else {
      res.status(404).json({ message: 'Not resultYEF' });
    }
  }

  static getById = async (req: Request, res: Response) => {
    const { id } = req.params;
    const userRepository = getRepository(Users);
    try {
      const user = await userRepository.findOneOrFail(id);
      res.send(user);
    } catch (e) {
      res.status(404).json({ message: 'Not resultRE' });
    }
  }

  static new = async (req: Request, res: Response) => {
    const { username, password, role } = req.body;
    const user = new Users();

    user.username = username;
    user.password = password;
    user.role = role;

    // Validate
    const validationOpt = { validationError: { target: false, value: false } };
    const errors = await validate(user, validationOpt);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }

    // TODO: HASH PASSWORD

    const userRepository = getRepository(Users);
    try {
      user.hashPassword();
      await userRepository.save(user);
    } catch (e) {
      return res.status(409).json({ message: 'Username ya existe' });
    }
    // TODO ok
    res.send('Usuario creado');
  }

  static edit = async (req: Request, res: Response) => {
    let user;
    const { id } = req.params;
    const { username, role } = req.body;

    const userRepository = getRepository(Users);
    // Try get user
    try {
      user = await userRepository.findOneOrFail(id);
      user.username = username;
      user.role = role;
    } catch (e) {
      return res.status(400).json({ message: 'User not foundaa' });
    }
    const validationOpt = { validationError: { target: false, value: false } };
    const errors = await validate(user, validationOpt);

    if (errors.length > 0) {
      return res.status(400).json(errors);
    }

    // intentar guardar usuario
    try {
      await userRepository.save(user);
    } catch (e) {
      return res.status(409).json({ message: 'Username esta en uso' });
    }

    res.status(201).json({ message: 'Usuario actualizado' });
  }

  static delete = async (req: Request, res: Response) => {
    const { id } = req.params;
    const userRepository = getRepository(Users);
    let user: Users;

    try {
      user = await userRepository.findOneOrFail(id);
    } catch (e) {
      return res.status(404).json({ message: 'User not foundaa ' });
    }

    // Remove user
    userRepository.delete(id);
    res.status(201).json({ message: ' Usuario eliminado' });
  }
}

export default UserController;
