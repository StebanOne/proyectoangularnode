import { checkRole } from './../middlewares/role';
import { checkJwt } from './../middlewares/jwt';
import { UserController } from './../controller/UserController';
import { Router } from 'express';

const router = Router();

// Get ver todos los usuarios
router.get('/', UserController.getAll);

// Get ver 1 usuario
router.get('/:id', UserController.getById);

// Create un nuevo usuario
router.post('/', UserController.new);

// Edit usuario
router.patch('/:id', UserController.edit);

// Delete
router.delete('/:id'/*, [checkJwt, checkRole(['admin'])]*/, UserController.delete);

export default router;
